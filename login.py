from flask import Flask, render_template,request


app = Flask(__name__)

@app.route('/')
@app.route('/<input>')
def hello(input=None):
    return render_template('login.html', name=input)

@app.route('/', methods=['GET', 'post'])
def view_form():
    if request.method == 'POST':
        username= request.form['username'];
        
        return render_template('index.html', name=username)
        
@app.route('/contact', methods=['GET', 'post'])
def contact():
    if request.method == 'POST':
        name= request.form['name'];
        email= request.form['email'];
        message= request.form['message'];

        return render_template('index.html', name=name)
        #email=email,message=message
if __name__ == '__main__':
    app.run(port=8080, host='0.0.0.0', debug=True)