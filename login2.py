from flask import Flask, render_template, request
import os

app = Flask(__name__)
app.config.update(dict(
    DATABASE=os.path.join(app.root_path, 'comments.db'),
    SECRET_KEY='development key'
))

@app.route('/')
@app.route('/<input>')
def hello(input=None):
    print("line 13")

    return render_template('login.html', name=input)

@app.route('/', methods=['GET', 'post'])
def view_form():
    if request.method == 'POST':
        name = request.form["name"]
        email = request.form["email"]
        comments = request.form["comments"]
        with sqlite3.connect(app.config['DATABASE']) as con:
            cur = con.cursor()
            cur.execute("INSERT INTO comments_table (name,email,comments) VALUES (?,?,?)", (name,email,comments))
            con.commit()
            print("line 25")

    return render_template('index.html', name=name)
    #username= request.form['username'];
    return render_template('index.html', name=name)
        
@app.route('/contact', methods=['GET', 'post'])
def contact():
    if request.method == 'POST':
        name= request.form['name'];
        email= request.form['email'];
        message= request.form['message'];

        return render_template('index.html', name=name)
        #email=email,message=message
if __name__ == '__main__':
    app.run(port=8080, host='0.0.0.0', debug=True)
    